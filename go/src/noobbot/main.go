package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"math"
	"net"
	"os"
	"strconv"
)

const CARID = "Duncan mates"

func connect(host string, port int) (conn net.Conn, err error) {
	conn, err = net.Dial("tcp", fmt.Sprintf("%s:%d", host, port))
	return
}

func read_msg(reader *bufio.Reader) (msg interface{}, err error) {
	var line string
	line, err = reader.ReadString('\n')
	if err != nil {
		return
	}
	// log.Printf(fmt.Sprintf("Got %s", line))
	err = json.Unmarshal([]byte(line), &msg)
	if err != nil {
		return
	}
	return
}

func write_msg(writer *bufio.Writer, msgtype string, data interface{}) (err error) {
	m := make(map[string]interface{})
	m["msgType"] = msgtype
	m["data"] = data
	var payload []byte
	payload, err = json.Marshal(m)
	_, err = writer.Write([]byte(payload))
	if err != nil {
		return
	}
	log.Printf(fmt.Sprintf("Send %s", payload))
	_, err = writer.WriteString("\n")
	if err != nil {
		return
	}
	writer.Flush()
	return
}

func send_join(writer *bufio.Writer, name string, key string) (err error) {
	// data := make(map[string]interface{})
	botId := make(map[string]interface{})
	botId["name"] = name
	botId["key"] = key
	// data["botId"] = botId
	// data["trackName"] = "germany"
	// data["carCount"] = "1"
	err = write_msg(writer, "join", botId)
	return
}

func send_ping(writer *bufio.Writer) (err error) {
	err = write_msg(writer, "ping", make(map[string]string))
	return
}

func send_throttle(writer *bufio.Writer, throttle float64) (err error) {
	err = write_msg(writer, "throttle", throttle)
	return
}

func send_steer(writer *bufio.Writer, where direction) (err error) {
	if where != straight {
		err = write_msg(writer, "switchLane", directions_encoded[where])
	}
	return
}

func send_boost(writer *bufio.Writer) (err error) {
	err = write_msg(writer, "turbo", "dajeeeee")
	return
}

func game_init(writer *bufio.Writer, data interface{}, t *Track) (err error) {
	// log.Println(data)
	send_ping(writer)
	// transform lanes
	jtrack := data.(map[string]interface{})["race"].(map[string]interface{})["track"].(map[string]interface{})
	jlanes := jtrack["lanes"].([]interface{})
	t.lanes = make([]Lane, len(jlanes))
	for i, v := range jlanes {
		v := v.(map[string]interface{})
		t.lanes[i] = Lane{uint(v["index"].(float64)),
			v["distanceFromCenter"].(float64)}
	}
	// transform pieces
	jpieces := jtrack["pieces"].([]interface{})
	t.pieces = make([]Piece, len(jpieces))
	for i, v := range jpieces {
		v := v.(map[string]interface{})
		var p Piece
		sw := v["switch"] != nil
		if angle, ok := v["angle"]; ok {
			// we have a turn
			t := Turn{angle.(float64), v["radius"].(float64),
				sw}
			p = t
		} else {
			s := Straigth{v["length"].(float64), sw}
			p = s
		}
		t.pieces[i] = p
	}
	log.Println("Lanes: ", t.lanes)
	log.Println("Pieces", t.pieces)
	return
}

func parsePositions(data interface{}) (poss []Position) {
	for _, v := range data.([]interface{}) {
		var pos Position
		jpos := v.(map[string]interface{})
		// ID
		jid := jpos["id"].(map[string]interface{})
		pos.id.name = jid["name"].(string)
		pos.id.color = jid["color"].(string)
		// angle
		pos.angle = jpos["angle"].(float64)
		// POS
		jposition := jpos["piecePosition"].(map[string]interface{})
		pos.position.pieceIndex = uint(jposition["pieceIndex"].(float64))
		pos.position.inPieceDistance = jposition["inPieceDistance"].(float64)
		// lane
		jlane := jposition["lane"].(map[string]interface{})
		pos.position.lane.startIndex = uint(jlane["startLaneIndex"].(float64))
		pos.position.lane.endIndex = uint(jlane["endLaneIndex"].(float64))
		pos.lap = uint(jposition["lap"].(float64))
		poss = append(poss, pos)
	}
	return
}

func positionDiff(a, b PiecePosition, track *Track) (dist float64) {
	if a.pieceIndex == b.pieceIndex {
		dist = a.inPieceDistance - b.inPieceDistance
	} else {
		oldPiece := track.pieces[b.pieceIndex]
		dist = a.inPieceDistance +
			(oldPiece.Length(b.lane.endIndex, track.lanes) - b.inPieceDistance)
	}
	return
}

func (current *State) print(old *State, track *Track) string {
	// format "pieceId throttle v acc angle vangle aangle lacc")
	var lateralAcceleration float64
	pieceIndex := current.position.position.pieceIndex
	piece := track.pieces[pieceIndex]
	if !piece.IsStraight() {
		radius := laneRadius(piece.(Turn),
			track.lanes[current.position.position.lane.endIndex])
		lateralAcceleration = math.Pow(current.speed, 2) / radius
	}
	aangle := current.dangle - old.dangle
	return fmt.Sprint(pieceIndex, current.throttle, current.speed, current.acc,
		current.position.angle, current.dangle, aangle,
		lateralAcceleration)
}

func (current *State) update(old *State, data interface{}, track *Track) {
	// get our position
	var newPos Position
	for _, p := range parsePositions(data) {
		if p.id.name == CARID {
			newPos = p
			break
		}
	}
	current.dangle = math.Abs(newPos.angle) - math.Abs(old.position.angle)
	travelDistance := positionDiff(newPos.position, old.position.position,
		track)
	current.speed = travelDistance
	current.acc = current.speed - old.speed
	current.distance = old.distance + travelDistance
	current.position = newPos
	current.turboAvailable = old.turboAvailable
}

func getThrottle(targetSpeed float64, current *State) float64 {
	k := float64(1)
	return k * (targetSpeed - current.speed)
}

func computeTargetSpeed(piece Turn, current *State, t *Track) float64 {
	radius := laneRadius(piece, t.lanes[current.position.position.lane.endIndex])
	return math.Sqrt(radius / 2.1) // + (5 / math.Abs(piece.angle))
}

func nextTurn(current *State, t *Track) (index int, distance float64) {
	currentIndex := int(current.position.position.pieceIndex)
	trackLen := len(t.pieces)
	distance = 0
	for i := 1; i < trackLen; i++ {
		index := int(math.Mod(float64(currentIndex+i), float64(trackLen)))
		if t.pieces[index].IsStraight() {
			distance += t.pieces[index].Length(current.position.position.lane.endIndex, t.lanes)
		} else {
			return index, distance +
				t.pieces[currentIndex].Length(current.position.position.lane.endIndex, t.lanes) -
				current.position.position.inPieceDistance
		}
	}
	return
}

func computeThrottle(old, current *State, track *Track) {
	currentIndex := current.position.position.pieceIndex
	currentPiece := track.pieces[currentIndex]
	deceleration := float64(-0.1)
	switch {
	case currentPiece.IsStraight():
		nextTurnIndex, nextTurnDistance := nextTurn(current, track)
		nextTurnTargetSpeed := computeTargetSpeed(track.pieces[nextTurnIndex].(Turn), current, track)
		decelerationTime := (nextTurnTargetSpeed - current.speed) / -0.1
		decelerationSpace := (current.speed * decelerationTime) +
			(1 / 2 * deceleration * math.Pow(decelerationTime, 2))
		if nextTurnDistance > decelerationSpace {
			current.throttle = 1
		} else {
			current.throttle = 0
		}
	case !currentPiece.IsStraight():
		nextTurnIndex, _ := nextTurn(current, track)
		nextTurnTargetSpeed := computeTargetSpeed(track.pieces[nextTurnIndex].(Turn), current, track)
		currentTargetSpeed := computeTargetSpeed(currentPiece.(Turn), current, track)
		current.throttle = getThrottle(math.Min(currentTargetSpeed, nextTurnTargetSpeed), current)
		if current.dangle < 0 {
			current.throttle += 0.02 * (30 - current.position.angle)
		}
	default:
		current.throttle = 1
	}
	if current.throttle < 0 {
		current.throttle = 0
	}
	if current.throttle > 1 {
		current.throttle = 1
	}
	return
}

func computeSteer(old, current *State, track *Track) {
	/*
	   I can send the steer command just for the next lane switch
	   If in the current segment I cannot swith lane I skip this command
	   otherwise I look forward and I decide what to do
	*/
	currentIndex := current.position.position.pieceIndex
	currentPiece := track.pieces[currentIndex]

	if !currentPiece.IsSwitchable() {
		return
	}
}

func computeBoost(current *State, t *Track) bool {
	if !current.turboAvailable {
		return false
	}
	currentIndex := current.position.position.pieceIndex
	trackLen := len(t.pieces)
	for i := uint(0); i < 5; i++ {
		index := math.Mod(float64(currentIndex+i), float64(trackLen))
		if !t.pieces[int(index)].IsStraight() {
			return false
		}
	}
	current.turboAvailable = false
	return true
}

func dispatch_msg(writer *bufio.Writer, msgtype string, data interface{},
	t *Track, old *State) (current *State, err error) {

	current = nil
	switch msgtype {
	case "join":
		log.Printf("Joined")
		send_ping(writer)
	case "gameInit":
		err = game_init(writer, data, t)
		send_ping(writer)
	case "gameStart":
		log.Printf("Game started")
		get_switch_choice(data)
		send_ping(writer)
	case "crash":
		log.Printf("Someone crashed")
		send_ping(writer)
	case "gameEnd":
		log.Printf("Game ended")
		// Exit is not strictly necessary?
		send_ping(writer)
	case "carPositions":
		current = new(State)
		current.steer = straight
		current.update(old, data, t)
		computeSteer(old, current, t)
		send_steer(writer, current.steer)
		if boost := computeBoost(current, t); boost {
			send_boost(writer)
		}
		computeThrottle(old, current, t)
		send_throttle(writer, current.throttle)
		fmt.Println("STATE", current.print(old, t))
		// reset steer
		current.steer = straight
	case "error":
		log.Printf(fmt.Sprintf("Got error: %v", data))
		send_ping(writer)
	case "turboAvailable":
		log.Printf("TURBO AVAILABLE!")
		log.Println(data)
		var tmp State
		tmp = *old
		tmp.turboAvailable = true
		current = &tmp
	default:
		log.Printf("Got msg type: %s", msgtype)
		send_ping(writer)
	}
	return
}

func parse_and_dispatch_input(writer *bufio.Writer, input interface{},
	old *State, track *Track) (s *State, err error) {
	switch t := input.(type) {
	default:
		err = errors.New(fmt.Sprintf("Invalid message type: %T", t))
		return
	case map[string]interface{}:
		var msg map[string]interface{}
		var ok bool
		msg, ok = input.(map[string]interface{})
		if !ok {
			err = errors.New(fmt.Sprintf("Invalid message type: %v", msg))
			return
		}
		switch msg["data"].(type) {
		default:
			s, err = dispatch_msg(writer, msg["msgType"].(string), nil,
				track, old)
			if err != nil {
				return
			}
		case interface{}:
			s, err = dispatch_msg(writer, msg["msgType"].(string),
				msg["data"].(interface{}), track, old)
			if err != nil {
				return
			}
		}
	}
	return
}

func bot_loop(conn net.Conn, name string, key string) (err error) {
	reader := bufio.NewReader(conn)
	writer := bufio.NewWriter(conn)
	states := make([]*State, 0, 1000)
	states = append(states, &State{0, 0, 0, 0, 0, Position{Id{"", ""}, 0,
		PiecePosition{0, 0, ChangeLane{0, 0}}, 0}, false, straight})
	var track *Track
	track = new(Track)
	send_join(writer, name, key)
	for {
		input, err := read_msg(reader)
		if err != nil {
			log_and_exit(err)
		}
		newState, err := parse_and_dispatch_input(writer, input,
			states[len(states)-1], track)
		if err != nil {
			log_and_exit(err)
		}
		if newState != nil {
			states = append(states, newState)
		}
	}
	return
}

func parse_args() (host string, port int, name string, key string, err error) {
	args := os.Args[1:]
	if len(args) != 4 {
		return "", 0, "", "", errors.New("Usage: ./run host port botname botkey")
	}
	host = args[0]
	port, err = strconv.Atoi(args[1])
	if err != nil {
		return "", 0, "", "", errors.New(fmt.Sprintf("Could not parse port value to integer: %v\n", args[1]))
	}
	name = args[2]
	key = args[3]

	return
}

func log_and_exit(err error) {
	log.Fatal(err)
	os.Exit(1)
}

func main() {

	host, port, name, key, err := parse_args()

	if err != nil {
		log_and_exit(err)
	}

	fmt.Println("Connecting with parameters:")
	fmt.Printf("host=%v, port=%v, bot name=%v, key=%v\n", host, port, name, key)

	conn, err := connect(host, port)

	if err != nil {
		log_and_exit(err)
	}

	defer conn.Close()

	err = bot_loop(conn, name, key)
}
