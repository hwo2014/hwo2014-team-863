package main

import (
	"math"
)

type direction int
var directions_encoded = []string{"Left", "Right"}
const (
    straight = -1
    left = iota
    right = iota
)


// probably useless
type Msg struct {
	data    map[string]interface{} `json:"data"`
	msgType string                 `json:"msgType"`
	gameId  string                 `json:"gameId"`
	tick    uint                   `json:"gameTick"`
}

type JMap map[string]interface{}

type Lane struct {
	index    uint
	distance float64
}

type Piece interface {
	Length(lane uint, lanes []Lane) float64
	IsStraight() bool
        IsSwitchable() bool
}

type Straigth struct {
	length     float64
	switchable bool
}

type Turn struct {
	angle      float64
	radius     float64
	switchable bool
}

type Track struct {
	pieces []Piece
	lanes  []Lane
}

type State struct {
	distance       float64
	speed          float64
	acc            float64
	dangle         float64
	throttle       float64
	position       Position
	turboAvailable bool
        steer          direction
}

func (piece Straigth) Length(lane uint, lanes []Lane) float64 {
	return piece.length
}

func (_ Straigth) IsStraight() bool {
	return true
}

func (piece Straigth) IsSwitchable() bool {
        return  piece.switchable
}

func laneRadius(piece Turn, lane Lane) (radius float64) {
	if piece.angle > 0 {
		radius = piece.radius - lane.distance
	} else {
		radius = piece.radius + lane.distance
	}
	return
}

func (piece Turn) Length(lane uint, lanes []Lane) float64 {
	radius := laneRadius(piece, lanes[lane])
	return (2 * math.Pi * radius * math.Abs(piece.angle)) / 360
}

func (piece Turn) IsStraight() bool {
	return false
}

func (piece Turn) IsSwitchable() bool {
        return  piece.switchable
}


type Position struct {
	id       Id
	angle    float64
	position PiecePosition
	lap      uint
}

type Id struct {
	name  string
	color string
}

type PiecePosition struct {
	pieceIndex      uint
	inPieceDistance float64
	lane            ChangeLane
}

type ChangeLane struct {
	startIndex uint
	endIndex   uint
}
