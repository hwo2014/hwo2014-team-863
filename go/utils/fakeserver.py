#!/usr/bin/env python

import sys
import SocketServer

usage = """
usage: %(script)s <PORT> <TRACEFILE>

e.g. %(script)s 9090 traces/06.txt

Run a fake test server sending messages read from the trace
"""

import re

def msg_generator(trace_filename):
    with file(trace_filename) as fobj:
        for line in fobj:
            tokens = line.rstrip().split(None, 3)
            if len(tokens) == 4:
                if "got" == tokens[2].lower() and "\"msgType\"" in tokens[3]:
                    yield tokens[3]


class FakeTestServer(SocketServer.BaseRequestHandler):
    def get_line(self):
        chunks = []
        while True:
            new_chunk = self.request.recv(1024)
            if not new_chunk:
                break
            i = new_chunk.find("\n")
            if i > -1:
                cmd = ''.join(chunks)
                cmd += new_chunk[:i+1]
                yield cmd
                chunks = [new_chunk[i+1:]]
            else:
                chunks.append(new_chunk)

    def handle(self):
        msgs = msg_generator(self.server.trace)
        # I assume \n is the cmd terminator
        try:
            for incoming in self.get_line():
                print "received", incoming
                outgoing = next(msgs)
                print "sending", outgoing
                self.request.sendall(outgoing + "\n")
        except StopIteration:
            print "done"
        print "bye"

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print usage % dict(script=sys.argv[0])
        sys.exit(1)

    port = int(sys.argv[1])
    trace = sys.argv[2]
    msgs = msg_generator(trace)
    server = SocketServer.TCPServer(("localhost", port), FakeTestServer)
    print "start server at localhost:%d" % port
    server.trace = trace
    server.serve_forever()



